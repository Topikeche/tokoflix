HOW TO RUN APPS TOKOFLIX

1. make sure your machine installed node.js version >= 8
2. clone tokoflix from repository
3. enter project directory by terminal
4. run "nmp install" wait for a moment
5. run the apps "npm start", there will be some errors, but its okay for now
6. open your browser and set browser local storage
   - chrome:
	 		- right click
			- inspect element
			- open tab application, if there's none, click ico ">>" to show some hidden tab
			- open LocalStorage > http://localhost:3000 and give em some data
					data:
					balance: 100000
					email: some other email
					name: some other name
					owned: {"172705":true,"394496":true,"467012":true,"516716":true,"566097":true,"566100":true,"572092":true,"573096":true}
	 - firefox: nearly same as chrome 
7. reload your browser again