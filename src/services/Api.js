import axios from 'axios'

export default {
	nowPlaying(baseURL){
		return {
			getPerPage: (page) => {
				return axios.get(`${baseURL}/movie/now_playing?api_key=0116bc973f8803d35fbf571480d2d7ec&language=en-US&region=ID&page=${page}`)
			}
		}
	},
	movie(baseURL){
		return {
			getDetailMovie: (id) => {
				return axios.get(`${baseURL}/movie/${id}?api_key=0116bc973f8803d35fbf571480d2d7ec&language=en-US&append_to_response=credits`)
			},
			getSimilarMovie: (id, page) => {
				return axios.get(`${baseURL}/movie/${id}/similar?api_key=0116bc973f8803d35fbf571480d2d7ec&language=en-US&page=${page}`)
			},
			getRecomendation: (id, page) => {
				return axios.get(`${baseURL}/movie/${id}/recommendations?api_key=0116bc973f8803d35fbf571480d2d7ec&language=en-US&page=${page}`)
			}
		}
	},
	genre(baseURL){
		return {
			getAllGenre: () => {
				return axios.get(`${baseURL}/genre/movie/list?api_key=0116bc973f8803d35fbf571480d2d7ec&language=en-US`)
			}
		}
	}

}