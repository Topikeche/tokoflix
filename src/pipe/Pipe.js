export const toExcerpt = (str, length) => {
	return str.substring(0, length)
}

export const getPrice = (rating) => {
	const R = parseInt(rating)
	if(R > 0 && R <= 3) return 3500
	else if(R > 3 && R <= 6) return 8250
	else if(R > 6 && R <= 8) return 16350
	else if(R > 8 && R <= 10) return 21250
	else return 1000
}

export const dateStringToDate = (str) => {
	const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
	const date = new Date(str)
	return date.getDate()+' '+months[date.getMonth()]+' '+date.getFullYear()
}

export const dateStringGetYear = (str) => {
	const date = new Date(str)
	return date.getFullYear()
}

export const minToHour = (min) => {
	let hour = Math.floor(min / 60)
	let minutes = min % 60
	return hour+'h '+minutes+'m'
}

export const createSlug = (str) => {
	return str.replace(/\s+/g, '-').toLowerCase()
}