import React from 'react'

const Footer = () => {
	return (
		<footer className="page-footer light-green darken-4">
      <div className="center-align">© 2018 TokoFLIX, all rights reserved.</div>
    </footer>
	)
}

export default Footer
