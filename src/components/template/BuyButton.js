import React, { Component } from 'react'
import { connect } from 'react-redux'
import M from 'materialize-css'
import { OWNED_ADD } from '../../reducers/actions/ownedAction'
import { BALANCE_UPDATE } from '../../reducers/actions/balanceAction'

class BuyButton extends Component {
	handleClick = () => {
		this.props.addOwned(this.props.id)
		this.props.updateBalance(this.props.price)
		M.toast({html: 'Movie Purchased'})
	}
	render() {
		let owned = (typeof this.props.owned[this.props.id] === 'undefined')  ? (false) : (true)
		return (
			<div>
				<button className="btn z-depth-0 amber darken-4" onClick={ this.handleClick } disabled={owned}>
					<i className="material-icons left">shopping_cart</i>Buy This
				</button>
			</div>
		)
	}
}

const mapStateToProps = (state) => {
	return{
		balance: state.user.balance,
		owned: state.user.owned
	}
}
const mapDispatchToProps = (dispatch) => {
	// buy movie means add movie to user owned collection and update balance
	return {
		addOwned: (id) => { dispatch(OWNED_ADD(id)) },
		updateBalance: (price) => { dispatch(BALANCE_UPDATE(price))}
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(BuyButton)
