import React from 'react'

const ItemCastVertical = ({ data }) => {
	// console.log(data)
	const profilePicture = data.profile_path ? (<img src={"https://image.tmdb.org/t/p/w138_and_h175_face/" + data.profile_path } alt={ data.name } className="circle" />) : (<i className="material-icons circle">person</i>)
	return (
		<li className="collection-item avatar">
			{ profilePicture }
			<span className="title"><b>{ data.name }</b></span>
			<p>as { data.character }</p>
			<a href="#!" className="secondary-content"><i className="material-icons">grade</i></a>
		</li>
	)
}

export default ItemCastVertical