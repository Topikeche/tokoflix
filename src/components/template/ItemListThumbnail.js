import React from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { toExcerpt, getPrice, dateStringToDate, createSlug } from '../../pipe/Pipe'

const ItemListThumbnail = ({ data, genres, owned}) => {

	let movGenre = ""
	if(typeof data.genre_ids !== 'undefined'){
		if(data.genre_ids.length) for(let i in data.genre_ids) movGenre += genres[data.genre_ids[i]] + ', '
		else movGenre = "N/A"
	}
	if(typeof data.genres !== 'undefined'){
		if(data.genres.length) for(let i in data.genres) movGenre += data.genres[i].name + ', '
		else movGenre = "N/A"
	}
	let label = (typeof owned[data.id] === 'undefined') ? (
		<span className="tag">IDR { getPrice(data.vote_average) },-</span>
	) : (
		<span className="tag purchased">Purchased</span>
	)

	return (
		<div className="col s12 m12 l6 xl6 no-padding">
			<Link to={ "/"+data.id+'-'+createSlug(data.title) }>
			<div className="card horizontal custom hoverable card-grid">
				<div className="card-image">
					<img src={ "http://image.tmdb.org/t/p/w185/"+data.poster_path } alt="zzz" />
					{ label }
				</div>
				<div className="card-stacked">
					<div className="card-content">
						<h5 className="no-margin text-green">{ data.title }</h5>
						<p className="genre">Genre: { movGenre }</p>
						<p>{ toExcerpt(data.overview,100) + '...' }</p>
						<div className="row no-margin meta">
							<div className="rating col s12 m2 l3 xl3 no-padding"><i className="material-icons text-yellow">star</i>&nbsp;{ data.vote_average }</div>
							<div className="date col s12 m10 l9 xl9 no-padding"><i className="material-icons">date_range</i>&nbsp;{ dateStringToDate(data.release_date) }</div>
						</div>
					</div>
				</div>
			</div>
			</Link>
		</div>
	)
	
}

const mapStateToProps = (state) => {
	return{
		owned: state.user.owned
	}
}

export default connect(mapStateToProps)(ItemListThumbnail)