import React from 'react'
import { Link } from 'react-router-dom'
import { createSlug } from '../../pipe/Pipe'

const ItemListHorizontal = ({ data }) => {
	// console.log(data)
	return (
		<div className="col s6 m3 l2 xl2 item-horizontal">
			<Link to={ "/"+data.id+'-'+createSlug(data.title) } key={ data.id }>
			<div className="card hoverable">
        <div className="card-image">
					<img src={ "http://image.tmdb.org/t/p/w342/"+data.poster_path } alt="poster"/>
        </div>
        <div className="card-content">
          <p className="truncate">{ data.title }</p>
        </div>
      </div>
			</Link>
		</div>
	)
}

export default ItemListHorizontal