import React, { Component } from 'react'
import M from 'materialize-css'
import { Link } from 'react-router-dom'

class Header extends Component {

	componentDidMount = () => {
		document.addEventListener('DOMContentLoaded', function() {
			var elems = document.querySelector('.dropdown-trigger');
			M.Dropdown.init(elems, {constrainWidth: false});
		});		
	}

	render(){
		return (
			<header className="navbar-fixed-custom">
				<nav>
					<div className="nav-wrapper light-green darken-4">
						<ul className="left">
							<li><a href="#!" data-target="slide-out" className="sidenav-trigger show-on-large"><i className="material-icons">menu</i></a></li>
						</ul>
						<Link to="/" className="brand-logo-custom"><img src="https://upload.wikimedia.org/wikipedia/commons/2/2b/Git-logo-white.svg" alt="logo"/></Link>
						<ul className="right">
							<li>
								<div className="row" id="topbarsearch">
								<form action="" className="browser-default right">
									<input id="search-input" placeholder="Search" type="text" className="browser-default search-field" name="q" aria-label="Search box" />
									<label htmlFor="search-input"><i className="material-icons search-icon">search</i></label> 
								</form>
								</div>
							</li>
							<li><a href='#!' className='dropdown-trigger' data-target='dropdown1'><i className="material-icons">more_vert</i></a></li>
							<ul id='dropdown1' className='dropdown-content'>
								<li><a href="#!"><i className="material-icons">person</i>Account</a></li>
								<li><a href="#!"><i className="material-icons">settings</i>Setting</a></li>
								<li className="divider" tabIndex="-1"></li>
								<li><a href="#!"><i className="material-icons">exit_to_app</i>Logout</a></li>
							</ul>
						</ul>
					</div>
				</nav>
			</header>
		)
	}

}

export default Header
