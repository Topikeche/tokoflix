import React, { Component } from 'react'
import ItemListHorizontal from '../template/ItemListHorizontal';
import BuyButton from '../template/BuyButton';
import ItemCastVertical from '../template/ItemCastVertical';
import API from '../../services/Api'
import M from 'materialize-css'
import { getPrice, dateStringGetYear, minToHour } from '../../pipe/Pipe'
import { connect } from 'react-redux'
import Loading from '../template/Loading';

class MovieDetail extends Component {

	state = {
		isLoading: true,
		detail: {},
		cast: [],
		crew: [],
		recommendation: [],
		similar: []
	}

	getID = (str) => {
		let id = str.substring(0,str.indexOf("-"))
		return id
	}
	getMovieDetail = (id) => {
		API.movie("https://api.themoviedb.org/3").getDetailMovie(id)
			.then(res => {
				this.setState({ 
					isLoading: false,
					detail: res.data,
					cast: res.data.credits.cast,
					crew: res.data.credits.crew
				})
			})
			.catch(error => {
				M.toast({html: error})
			})
	}
	getMovieRecommendation = (id, page) => {
		API.movie("https://api.themoviedb.org/3").getRecomendation(id, page)
			.then(res => {
				this.setState({ 
					isLoading: false,
					recommendation: res.data.results
				})
			})
			.catch(error => {
				M.toast({html: error})
			})
	}
	getMovieSimilar = (id, page) => {
		API.movie("https://api.themoviedb.org/3").getSimilarMovie(id, page)
			.then(res => {
				this.setState({ 
					isLoading: false,
					similar: res.data.results
				})
			})
			.catch(error => {
				M.toast({html: error})
			})
	}

	componentDidMount = () => {		
		let id = this.getID(this.props.match.params.id)
		this.getMovieDetail(id)
		this.getMovieRecommendation(id, 1)
		this.getMovieSimilar(id, 1)
	}
	componentWillReceiveProps = (nextProps) => {
		let id = this.getID(nextProps.match.params.id)
		this.getMovieDetail(id)
		this.getMovieRecommendation(id, 1)
		this.getMovieSimilar(id, 1)
	}

	render() {

		let crews = this.state.crew.length ? (
			this.state.crew.slice(0,2).map(a => {
				return (
					<div key={a.id}><b>{ a.name }</b><br/>{ a.job }</div>
				)
			})
		) : (<div></div>)
		let casts = this.state.cast.length ? (
			this.state.cast.slice(0,5).map(a => {
				return <ItemCastVertical data={a} key={a.id}/>
			})
		) : (<div className="center">No cast are available</div>)
		let recommendations = this.state.recommendation.length ? (
			this.state.recommendation.slice(0,6).map(a => {
				return <ItemListHorizontal data={a} key={a.id} />
			})
		) : (<p className="center">No recomendation are available</p>)
		let similars = this.state.similar.length ? (
			this.state.similar.slice(0,6).map(a => {
				return <ItemListHorizontal data={a} key={a.id} />
			})
		) : (<p className="center">No similar movie are available</p>)
		let label = (typeof this.props.owned[this.state.detail.id] === 'undefined') ? (
			<span className="tag-medium">IDR { getPrice(this.state.detail.vote_average) },-</span>
		) : (
			<span className="tag-medium purchased">Purchased</span>
		)
		
		return (
			<div className="container movie-detail">
				<div className={this.state.isLoading ? '' : 'hidden'}>
					<Loading></Loading>
				</div>
				<div className={this.state.isLoading ? 'hidden' : ''}>
					<div className="heading row no-margin">
						<div className="col l4 xl4 no-padding poster">
							<img className="mov-poster" src={ "http://image.tmdb.org/t/p/w342/"+this.state.detail.poster_path } alt="zzz" />
							{ label }
						</div>
						<div className="col l8 xl8 no-padding">
							<div className="mov-title text-green">{ this.state.detail.title } <span className="year">({ dateStringGetYear(this.state.detail.release_date) })</span></div>
							<div className="meta">
								<div className="mov-rating"><i className="material-icons text-yellow">star</i>&nbsp;<b>{ this.state.detail.vote_average }</b></div>
								<div className="mov-duration"><i className="material-icons text-green">timer</i>&nbsp;<b>{ minToHour(this.state.detail.runtime) }</b></div>
								<div className="mov-vote"><i className="material-icons blue-text">thumb_up</i>&nbsp;<b>{ this.state.detail.vote_count }</b></div>
							</div>
							<h5><b>Overview</b></h5>
							<div className="overview">{ this.state.detail.overview }</div>
							<h5><b>Featured Crew</b></h5>
							<div className="mov-crew">
								{ crews }
								<div><BuyButton id={ this.state.detail.id } price={ getPrice(this.state.detail.vote_average) }></BuyButton></div>
							</div>
						</div>
					</div>
					<div className="mov-cast">
						<h5><b>Cast</b></h5>
						<hr/>
						<ul className="collection">
							{ casts }
						</ul>
					</div>
					<div className="mov-similar">
						<h5><b>Similar Movies</b></h5>
						<hr/>
						<div className="row no-margin">
							{ similars }
						</div>
					</div>
					<div className="mov-recommend">
					<h5><b>Recommendation Movies</b></h5>
						<hr/>
						<div className="row no-margin">
							{ recommendations }
						</div>
					</div>
				</div>				
			</div>
		)
	}
}

const mapStateToProps = (state) => {
	return{
		owned: state.user.owned
	}
}

export default connect(mapStateToProps)(MovieDetail)
