import React, { Component } from 'react'
import API from '../../services/Api'
import ItemListThumbnail from '../template/ItemListThumbnail'
import M from 'materialize-css'
import Loading from '../template/Loading'
import Pagination from "react-js-pagination"

class Home extends Component {

	state = {
		pagination: {
			itemPerPage: 20,
			activePage: null,
			itemCount: null
		},
		isLoading: true,
		nowPlaying: [],
		genres: {},
		baseUrl: "https://api.themoviedb.org/3"
	}

	getNowPlaying = (page) => {
		API.nowPlaying(this.state.baseUrl).getPerPage(page)
			.then(res => {
				this.setState({ 
					pagination: {
						...this.state.pagination,
						activePage: res.data.page,
						itemCount: res.data.total_results
					},
					nowPlaying: res.data.results, 
					isLoading: false 
				})
			})
			.catch(error => {
				console.log(error)
				M.toast({html: error})
			});
	}
	getGenreList = () => {
		API.genre(this.state.baseUrl).getAllGenre()
			.then(res => {
				let newGenres = {}
				for(let i in res.data.genres) newGenres[res.data.genres[i].id] = res.data.genres[i].name
				this.setState({ genres: newGenres, isLoading: false })
			})
			.catch(error => {
				console.log(error)
				M.toast({html: error})
			})
	}
	handlePageChange = (pageNumber) => {
		this.getNowPlaying(pageNumber)
		this.scrollToTop(700)
	}
	scrollToTop = (scrollDuration) => {
    let scrollStep = -window.scrollY / (scrollDuration / 15),
        scrollInterval = setInterval(function(){
        if ( window.scrollY !== 0 ) {
            window.scrollBy( 0, scrollStep );
        }
        else clearInterval(scrollInterval); 
    },15);
}

	componentDidMount = () => {
		this.getNowPlaying(1)
		this.getGenreList()
	}

	render() {
		let movieList = this.state.nowPlaying.length ? (
			this.state.nowPlaying.map(a => {
				return(<ItemListThumbnail key={a.id} data={a} genres={this.state.genres}/>)
			})
		) : (<div className="center"><b>SORRY</b>, Unfortunetly There's No Movies Airing Right Now</div>)

		return (
			<div className="container">
				<div className={this.state.isLoading ? '' : 'hidden'}>
					<Loading className="hiden"></Loading>
				</div>
				<div className={this.state.isLoading ? 'hidden' : ''}>
					<h4 className="no-margin">NOW AIRING in <b className="text-green">INDONESIA</b></h4>
					<div className="gap-10"></div>
					<div className="row">					
						{ movieList }
					</div>
				</div>
				<div className="center-align">
					<Pagination
						activePage={this.state.pagination.activePage}
						itemsCountPerPage={this.state.pagination.itemPerPage}
						totalItemsCount={this.state.pagination.itemCount}
						pageRangeDisplayed={10}
						onChange={this.handlePageChange}
					/>
				</div>
			</div>
		)
	}
}

export default Home
