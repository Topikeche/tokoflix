import React, { Component } from 'react'
import { connect } from 'react-redux'
import API from '../../services/Api'
import ItemListThumbnail from '../template/ItemListThumbnail'
import M from 'materialize-css'
import Loading from '../template/Loading';

class MovieOwned extends Component {

	state = {
		isLoading: true,
		movie: [],
		genres: {}
	}

	getMovieDetail = (id) => {
		let newData = this.state.movie
		API.movie("https://api.themoviedb.org/3").getDetailMovie(id)
			.then(res => {
				newData.push(res.data)
				this.setState({ movie: newData })
			})
			.catch(error => {
				M.toast({ html: error })
			})
	}

	componentDidMount = () => {
		Object.keys(this.props.owned).forEach(key => {
			this.getMovieDetail(key)
			this.setState({ isLoading: false })
		});
	}

	render() {

		let movieList = this.state.movie.length ? (
			this.state.movie.map(a => {
				return(<ItemListThumbnail key={a.id} data={a} genres={this.state.genres}/>)
			})
		) : (<div className="center">You Haven't Purchased Any Single Movie</div>)

		return (
			<div className="container">
				<div className={this.state.isLoading ? '' : 'hidden'}>
					<Loading></Loading>
				</div>
				<div className={this.state.isLoading ? 'hidden' : ''}>
					<h4 className="no-margin">OWNED <b className="text-green">MOVIE</b></h4>
					<div className="gap-10"></div>
					<div className="row">					
						{ movieList }
					</div>
				</div>
			</div>
		)
	}

}

const mapStateToProps = (state) => {
	return {
		owned: state.user.owned
	}
}

export default connect(mapStateToProps)(MovieOwned)
