import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import M from 'materialize-css'
import avatar from '../assets/images/avatar.png'
import sidenavBg from '../assets/images/sidenav-bg.jpg'
import { connect } from 'react-redux'

class Sidenav extends Component {

	componentDidMount = () => {
		document.addEventListener('DOMContentLoaded', function() {
			const options = {
				edge: 'left',
				draggable: true
			}
			var elems = document.querySelector('.sidenav');
			M.Sidenav.init(elems, options);		
		})
	}

	render() {
		// console.log(this.props)
		return (
				<ul id="slide-out" className="sidenav z-depth-3">
					<li><div className="user-view">
						<div className="background">
							<img src={ sidenavBg } alt="Background Box" />
						</div>
						<span className="balance">IDR { this.props.balance },-</span>
						<a href="#user"><img className="circle" src={ avatar } alt="Profile User" /></a>
						<a href="#name"><span className="white-text name">{ this.props.name }</span></a>
						<a href="#email"><span className="white-text email">{ this.props.email }</span></a>
					</div></li>
					<li>
						<NavLink to="/owned">
						<i className="material-icons">local_mall</i>Purchased Movie<span className="new badge" data-badge-caption="item">{ Object.keys(this.props.owned).length }</span>
						</NavLink>
					</li>
				</ul>
		)
	}

}

const mapStateToProps = (state) => {
	return{
		name: state.user.name,
		email: state.user.email,
		balance: state.user.balance,
		owned: state.user.owned
	}
}

export default connect(mapStateToProps)(Sidenav)
