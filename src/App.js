import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Home from './components/pages/Home';
import MovieDetail from './components/pages/MovieDetail';
import Sidenav from './components/Sidenav';
import Header from './components/Header';
import Footer from './components/Footer';
import './App.css';
import MovieOwned from './components/pages/MovieOwned';

class App extends Component {
  componentWillMount = () => {
    localStorage.setItem('name', "My Name Is Khan")
    localStorage.setItem('email', "ohmykhan@mail.com")
    localStorage.setItem('balance', 100000)
    localStorage.setItem('owned', '{"172705":true,"394496":true,"467012":true,"516716":true,"566097":true,"566100":true,"572092":true}')
  }
  render() {
    const ScrollToTop = () => {
      window.scrollTo(0, 0);
      return null;
    };
    return (
      <BrowserRouter>
        <div className="App">
          <div>
            <Header/>
            <Sidenav/>
            <main>
              <Route component={ScrollToTop} />
              <Switch>
                <Route exact path="/" component={ Home } />
                <Route exact path="/owned" component={ MovieOwned } />
                <Route path="/:id" component={ MovieDetail } />
              </Switch>
            </main>
            <Footer/>
          </div>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
