export const BALANCE_UPDATE = (price) => {
	return {
		type: "BALANCE_UPDATE",
		price: price,
	}
}