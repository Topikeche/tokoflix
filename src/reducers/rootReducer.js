const initState = {
	user: {
		name: localStorage.getItem('name'),
		email: localStorage.getItem('email'),
		balance: localStorage.getItem('balance'),
		owned: JSON.parse(localStorage.getItem('owned'))
	}
}

function updateLocalStorage(key,value){
	localStorage.setItem(key, value)
}
function getLocalStorage(){
	return {
		user: {
			name: localStorage.getItem('name'),
			email: localStorage.getItem('email'),
			balance: localStorage.getItem('balance'),
			owned: JSON.parse(localStorage.getItem('owned'))
		}
	}
}
function addOwned(state, id){
	let newOwned = state.user.owned
	newOwned[id] = true
	updateLocalStorage('owned', JSON.stringify(newOwned))
	return getLocalStorage()
}
function purchase(state,price){
	let newbalance = (state.user.balance - price)
	updateLocalStorage('balance', newbalance)
	return getLocalStorage()
}

const rootReducer = (state = initState, action) => {
	if(action.type === "OWNED_ADD") return addOwned(state, action.id)
	if(action.type === "BALANCE_UPDATE") return purchase(state, action.price)
	return state
}

export default rootReducer